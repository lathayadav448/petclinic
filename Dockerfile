FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
#ARG buildtime_variable=default_value
ENV spring.profiles.active=mysql
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]


